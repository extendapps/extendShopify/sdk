/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import {Order} from '@extendapps/shopifytypes/Objects/Order';
import {ShopifyEventDetails} from '@extendapps/shopifytypes/Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from '@extendapps/shopifytypes/Interfaces/Shopify_TopicHandler';
import * as log from 'N/log';

export let cancelled: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - cancelled (CUSTOM)', order);
};

export let create: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - create (CUSTOM)', order);
};

// tslint:disable-next-line:variable-name
export let _delete: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - delete (CUSTOM)', order);
};

export let fulfilled: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - fulfilled (CUSTOM)', order);
};

export let paid: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - paid (CUSTOM)', order);
};

// tslint:disable-next-line:variable-name
export let partially_fulfilled: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - partially_fulfilled (CUSTOM)', order);
};

export let updated: ShopifyTopicHandler.Order.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - updated (CUSTOM)', order);
};
