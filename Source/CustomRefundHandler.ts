/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {Refund} from '@extendapps/shopifytypes/Objects/Refund';
import {ShopifyEventDetails} from '@extendapps/shopifytypes/Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from '@extendapps/shopifytypes/Interfaces/Shopify_TopicHandler';

export let create: ShopifyTopicHandler.Refund.handler = (eventDetails: ShopifyEventDetails, refund: Refund) => {
    log.audit('refund - create (CUSTOM)', refund);
};
