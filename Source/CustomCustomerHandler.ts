/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {Customer} from '@extendapps/shopifytypes/Objects/Customer';
import {ShopifyEventDetails} from '@extendapps/shopifytypes/Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from '@extendapps/shopifytypes/Interfaces/Shopify_TopicHandler';

export let create: ShopifyTopicHandler.Customer.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - create (CUSTOM)', customer);
};

// tslint:disable-next-line:variable-name
export let _delete: ShopifyTopicHandler.Customer.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - delete (CUSTOM)', customer);
};

export let disable: ShopifyTopicHandler.Customer.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - disable (CUSTOM)', customer);
};

export let enable: ShopifyTopicHandler.Customer.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - enable (CUSTOM)', customer);
};

export let update: ShopifyTopicHandler.Customer.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - update (CUSTOM)', customer);
};
