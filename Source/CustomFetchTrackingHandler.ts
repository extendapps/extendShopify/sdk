/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

export let fetchTrackingNumbers: FetchTrackingNumbersFunction = (orderNames: string[], shop: string) => {
    // Example Return Payload
    return {
        tracking_numbers: {
            '#1001.1': 'qwerty',
            '#1002.1': 'asdfg',
            '#1003.2': 'zxcvb'
        },
        message: 'Successfully received the tracking numbers',
        success: true
    };
};

export type FetchTrackingNumbersFunction = (orderNames: string[], shop: string) => any;
